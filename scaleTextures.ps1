# This script is intended to be run from release.ps1
param (
    [string]$currentFolder = "",
    [string]$filter = ""
)
$ddsinfo = "C:\Program Files\NVIDIA Corporation\NVIDIA Texture Tools\nvddsinfo.exe"
$ddscompress = "C:\Program Files\NVIDIA Corporation\NVIDIA Texture Tools\nvcompress.exe"
$ddsdecompress = "C:\Program Files\NVIDIA Corporation\NVIDIA Texture Tools\nvdecompress.exe"
if ($currentFolder.EndsWith(".dds")) { # Scale single texture
    $path = $currentFolder
    $pathPng = $path.Replace(".dds", ".png")
    $info = &$ddsinfo $path
    $info = $info -join "`n"
    $compression = ""
    $alpha = ""
    $mipmaps = ""
    if ($info.Contains("DDPF_LUMINANCE")) {
        &magick.exe $path $pathPng # decompress via imagemagick if heightmap
    } else {
        &$ddsdecompress -format png $path
    }
    $alphaChannel = &magick.exe identify -format '%A' $pathPng
    if ($alphaChannel -ne "Undefined" -and -not $info.Contains("DDPF_LUMINANCE")) {
        &magick.exe $pathPng -channel RGBA -separate -resize 50% -combine $pathPng # Decompose because ImageMagick doesn't like when the alpha is 0
    } else {
        &magick.exe mogrify -resize 50% $pathPng
    }
    if ($info.Contains("Mipmap count: 1`n")) {
        $mipmaps = "-nomips"
    }
    if ($info.Contains("FourCC: 'DXT5'")) {
        $compression = "-bc3"
        $alpha = "-alpha"
    } elseif ($info.Contains("FourCC: 'DXT1'")) {
        $compression = "-bc1"
    } elseif ($info.Contains("DDPF_LUMINANCE")) { # L8 heightmap
        $compression = "-lumi"
    }
    &$ddscompress $alpha $compression $mipmaps $pathPng
    Remove-Item $pathPng
} elseif ($currentFolder.EndsWith("PluginData")) { # KSRSS Textures, scale only 16k
    $ddsTextures = Get-ChildItem -Path $currentFolder\*.dds -Name
    foreach ($t in $ddsTextures) {
        $path = "$currentFolder\$t"
        $pathPng = $path.Replace(".dds", ".png")
        $info = &$ddsinfo $path
        $info = $info -join "`n"
        $compression = ""
        $alpha = ""
        $mipmaps = ""
        if ($info.Contains("Width: 16384") -or $info.Contains("Width: 16200")) {
            if ($info.Contains("DDPF_LUMINANCE")) {
                &magick.exe $path $pathPng # decompress via imagemagick if heightmap
            } else {
                &$ddsdecompress -format png $path
            }
            $alphaChannel = &magick.exe identify -format '%A' $pathPng
            if ($alphaChannel -ne "Undefined" -and -not $info.Contains("DDPF_LUMINANCE")) {
                &magick.exe $pathPng -channel RGBA -separate -resize 50% -combine $pathPng # Decompose because ImageMagick doesn't like when the alpha is 0
            } else {
                &magick.exe mogrify -resize 50% $pathPng
            }
            if ($info.Contains("Mipmap count: 1`n")) {
                $mipmaps = "-nomips"
            }
            if ($info.Contains("FourCC: 'DXT5'")) {
                $compression = "-bc3"
                $alpha = "-alpha"
            } elseif ($info.Contains("FourCC: 'DXT1'")) {
                $compression = "-bc1"
            } elseif ($info.Contains("DDPF_LUMINANCE")) { # L8 heightmap
                $compression = "-lumi"
            }
            &$ddscompress $alpha $compression $mipmaps $pathPng
            Remove-Item $pathPng
        }
    }
} elseif ($currentFolder.EndsWith("Earth")) { #Earth EVE Textures, scale by 25%
    $ddsTextures = Get-ChildItem -Path $currentFolder\$filter -Name
    foreach ($t in $ddsTextures) {
        $path = "$currentFolder\$t"
        $pathPng = $path.Replace(".dds", ".png")
        $info = &$ddsinfo $path
        $info = $info -join "`n"
        $compression = ""
        $alpha = ""
        $mipmaps = ""
        if ($info.Contains("DDPF_LUMINANCE")) {
            &magick.exe $path $pathPng # decompress via imagemagick if heightmap
        } else {
            &$ddsdecompress -format png $path
        }
        $alphaChannel = &magick.exe identify -format '%A' $pathPng
        if ($alphaChannel -ne "Undefined" -and -not $info.Contains("DDPF_LUMINANCE")) {
            &magick.exe $pathPng -channel RGBA -separate -resize 25% -combine $pathPng # Decompose because ImageMagick doesn't like when the alpha is 0
        } else {
            &magick.exe mogrify -resize 25% $pathPng
        }
        if ($info.Contains("Mipmap count: 1`n")) {
            $mipmaps = "-nomips"
        }
        if ($info.Contains("FourCC: 'DXT5'")) {
            $compression = "-bc3"
            $alpha = "-alpha"
        } elseif ($info.Contains("FourCC: 'DXT1'")) {
            $compression = "-bc1"
        } elseif ($info.Contains("DDPF_LUMINANCE")) { # L8 heightmap
            $compression = "-lumi"
        }
        &$ddscompress $alpha $compression $mipmaps $pathPng
        Remove-Item $pathPng
    }
} else { # EVE Textures, scale all of the textures
    $ddsTextures = Get-ChildItem -Path $currentFolder\$filter -Name
    foreach ($t in $ddsTextures) {
        $path = "$currentFolder\$t"
        $pathPng = $path.Replace(".dds", ".png")
        $info = &$ddsinfo $path
        $info = $info -join "`n"
        $compression = ""
        $alpha = ""
        $mipmaps = ""
        if ($info.Contains("DDPF_LUMINANCE")) {
            &magick.exe $path $pathPng # decompress via imagemagick if heightmap
        } else {
            &$ddsdecompress -format png $path
        }
        $alphaChannel = &magick.exe identify -format '%A' $pathPng
        if ($alphaChannel -ne "Undefined" -and -not $info.Contains("DDPF_LUMINANCE")) {
            &magick.exe $pathPng -channel RGBA -separate -resize 50% -combine $pathPng # Decompose because ImageMagick doesn't like when the alpha is 0
        } else {
            &magick.exe mogrify -resize 50% $pathPng
        }
        if ($info.Contains("Mipmap count: 1`n")) {
            $mipmaps = "-nomips"
        }
        if ($info.Contains("FourCC: 'DXT5'")) {
            $compression = "-bc3"
            $alpha = "-alpha"
        } elseif ($info.Contains("FourCC: 'DXT1'")) {
            $compression = "-bc1"
        } elseif ($info.Contains("DDPF_LUMINANCE")) { # L8 heightmap
            $compression = "-lumi"
        }
        &$ddscompress $alpha $compression $mipmaps $pathPng
        Remove-Item $pathPng
    }
}
